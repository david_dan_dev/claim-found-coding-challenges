from libs import RequiredDocTypes as rdt

class User:
    def __init__(self, first_name, last_name, claims, submitted_documents):
        self.first_name = first_name
        self.last_name = last_name
        self.claims = claims
        self.submitted_documents = submitted_documents
        self.submitted_documents_by_type = {}

        self.__set_submitted_documents_by_type()

# Reference for pet_claim = False
    def __set_submitted_documents_by_type(self):
        for doc in self.submitted_documents:
            doc_type = doc['doc_type']
            doc_list = None

            if not doc['doc_type'] in self.submitted_documents_by_type:
                doc_list = self.submitted_documents_by_type[doc_type] = []
            else:
                doc_list = self.submitted_documents_by_type[doc_type]

            doc_list.append(doc)

    def has_submitted_doc_type(self, type):
        return type in self.submitted_documents_by_type

    def get_claim_desc_dict(self):
        dict = {}

        for claim in self.claims:
            dict[claim['claim_id']] = rdt.str_from_claim(claim, self)

        return dict
