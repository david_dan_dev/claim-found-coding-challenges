def _identity_str(claim, user):
    return 'Proof of identity'

def _address_str(claim, user):
    if not claim['address']:
        return None

    return 'Proof you lived at %s' % (claim['address'])

def _social_str(claim, user):
    if claim['address']:
        return None

    return 'Proof of social'

required_doc_types = [
    {
        "condition": "true",
        "doc_type": "identity",
        "per_claim": False,
        "state": "FL",
        "str": _identity_str
    },
    {
        "condition": "address",
        "doc_type": "address",
        "per_claim": True,
        "state": "FL",
        "str": _address_str
    },
     {
        "condition": "!address",
        "doc_type": "social",
        "per_claim": False,
        "state": "FL",
        "str": _social_str
    }
]

def str_from_claim(claim, user):
    str_list = []

    for req_doc_type in required_doc_types:
        condition = req_doc_type['condition']
        doc_type = req_doc_type['doc_type']

        if not req_doc_type['per_claim'] and user.has_submitted_doc_type(doc_type):
            # User has submitted doc_type, so no need to print
            continue

        # doc_type method to generate relevant string. If conditions not matched ,then None is returned
        desc = req_doc_type['str'](claim, user)

        if desc:
            str_list.append(desc)

    return ', '.join(str_list)
