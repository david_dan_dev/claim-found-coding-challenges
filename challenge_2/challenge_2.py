#!/usr/bin/python

from models.User import User

user_claims = [
    {
        "claim_id": "FL163650",
        "state": "FL",
        "company": "SPRINT FLORIDA INCORPORATED",
        "address": "123 Main St, NAPLES, FL 12345"
    },
    {
        "claim_id": "FL123444",
        "state": "FL",
        "company": "ABC CORP",
        "address": ""
    }
]

# User's submitted docs - if user has already submitted a required doc_type of per_claim = False then the required doc_type is ignored
user_docs = []

user = User('David', 'Dan', user_claims, user_docs)

print user.get_claim_desc_dict()
