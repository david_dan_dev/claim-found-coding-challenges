#!/usr/bin/python

from models.User import User

data = {
    'Create Profile': [{
            'Full First Name': 'John',
            'Last Name': 'Smith',
        }
    ],
    'States of Residency': [{
            'State': ['FL','VA']
        }
    ],
    'Create Account': [{
            'Email Address': 'thisisanemail',
            'Password': 'abc123',
        }
    ]
}

user = User.from_data(data)

# Default state is index 0 of states
print 'State: %s' % (user.state)

print repr(user)
