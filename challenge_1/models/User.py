#!/usr/bin/python

class User:
    def __init__(self, first_name, last_name, states, email, password):
        self.first_name = first_name
        self.last_name = last_name
        self.states = states
        self.email = email
        self.password = password

    def __repr__(self):
        return '<User> first_name={}, last_name={}>'.format(self.first_name, self.last_name)

    def __get_state(self):
        return self.states[0] if self.states and type(self.states) == list and len(self.states) else 'N/A'

    def __getattr__(self, name):
        if name == 'state':
            return self.__get_state()

        return self[name]

    @staticmethod
    def from_data(data):
        profile_data = data['Create Profile'][0]
        state_data = data['States of Residency'][0]
        account_data = data['Create Account'][0]

        first_name = profile_data['Full First Name']
        last_name = profile_data['Last Name']
        states = state_data['State']
        email = account_data['Email Address']
        password = account_data['Password']

        return User(first_name, last_name, states, email, password)
